MAC
_________________
ModID: 2479558100
____________________________
Miscreated Advanced Crafting

Little tweaked crafting...
_______________
Added recipes:


Hidden Stash

Medical Heal Joint

Medical Antibiotics Joint

Medical Antiradiation Joint

ZipZap Papers

Cannabis Vookie Dough

Cannabis Butter

Pie

Cookie Dough

Iron Ingot from 2 Scrap Metal

Leather repair kit from 2 WolfSkin


And Baking Guide
____________________________________________________________
![MAC](https://gitlab.com/miscreated/MAC/-/raw/MAC/MAC.jpg)
____________________________________________________________
Medical Cannabis thanks to: https://github.com/Bealze/Gamer-Love

Hidden Stash thanks to: https://steamcommunity.com/id/PitiViers

Steam Mod Link: https://steamcommunity.com/sharedfiles/filedetails/?id=2479558100

Feel free to contribute to project:
https://gitlab.com/miscreated/MAC

Tweak It! Share it! Enjoy it!

Check also MACE version:

https://steamcommunity.com/sharedfiles/filedetails/?id=2480835664
or on GitLab:
https://gitlab.com/miscreated/MACE
